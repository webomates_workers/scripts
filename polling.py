import requests
import json
import os
TEAM = 'webomates_workers'

def get_data(url):
  response = requests.get(url)
  content = json.loads(response.content)
  return content

def get_projects(TEAM):
  url = 'https://api.bitbucket.org/2.0/teams/%(TEAM)s/projects/'%locals()
  response = get_data(url)
  projects = []
  print "Total "+ str(len(response['values'])) +" projects find for %(TEAM)s team"%locals()
  for project in response['values']:
    key = project['key']
    print 'Project key is %(key)s for team %(TEAM)s'%locals()
    projects.append(key)
  return projects

def get_all_repos(TEAM,projects):
  repos_clone_url = []
  for project in projects:  
    url = 'https://api.bitbucket.org/2.0/repositories/%(TEAM)s?project.key=%(project)s'%locals()
    response = get_data(url)
    print "\nTotal "+ str(len(response['values'])) +" repos find for %(project)s project"%locals()
    for repo in response['values']:
      name = repo['name']
      repo_link = repo['links']['clone'][0]['href']
      print "Repository find for project %(project)s is %(name)s"%locals()
      branches_data = get_data(repo['links']['branches']['href'])['values']
      print "\nTotal "+ str(len(branches_data))+" branch found for this repo"
      for branch in branches_data:
        print branch['name']
        branch_name = branch['name']
        repos_clone_url.append('-b %(branch_name)s %(repo_link)s %(name)s-%(branch_name)s'%locals())        
  return repos_clone_url

def clone_repos(repos_url):
  for url in repos_clone_url:
    # print url
    # print 'git clone %(url)s'%locals()
    os.system('git clone %(url)s'%locals()) 

projects = get_projects(TEAM)
repos_clone_url = get_all_repos(TEAM,projects)

clone_repos(repos_clone_url)